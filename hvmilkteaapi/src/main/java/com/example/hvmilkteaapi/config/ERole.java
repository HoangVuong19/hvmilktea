package com.example.hvmilkteaapi.config;

public enum ERole {
    ROLE_ADMIN,
    ROLE_USER
}
