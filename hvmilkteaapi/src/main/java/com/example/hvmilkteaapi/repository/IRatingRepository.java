package com.example.hvmilkteaapi.repository;

import com.example.hvmilkteaapi.entity.Rating;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRatingRepository extends JpaRepository<Rating, Long> {
    Page<Rating> findRatingByUsernameLike(String keyword, Pageable pageable);


}
