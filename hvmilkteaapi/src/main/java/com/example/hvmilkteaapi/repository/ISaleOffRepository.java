package com.example.hvmilkteaapi.repository;

import com.example.hvmilkteaapi.entity.SaleOff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISaleOffRepository extends JpaRepository<SaleOff, Long> {
}
