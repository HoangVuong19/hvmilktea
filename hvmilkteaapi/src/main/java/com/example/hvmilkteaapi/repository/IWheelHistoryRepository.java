package com.example.hvmilkteaapi.repository;

import com.example.hvmilkteaapi.entity.WheelHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IWheelHistoryRepository extends JpaRepository<WheelHistory, Long> {

}
