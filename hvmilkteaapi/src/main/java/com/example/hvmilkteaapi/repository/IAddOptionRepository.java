package com.example.hvmilkteaapi.repository;

import com.example.hvmilkteaapi.entity.AdditionOption;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IAddOptionRepository extends JpaRepository<AdditionOption, Long> {
    Page<AdditionOption> findAdditionByNameLike(String keyword, Pageable pageable);
}
