package com.example.hvmilkteaapi.repository;

import com.example.hvmilkteaapi.entity.Spinner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ISpinnerRepository extends JpaRepository<Spinner, Long> {
}
