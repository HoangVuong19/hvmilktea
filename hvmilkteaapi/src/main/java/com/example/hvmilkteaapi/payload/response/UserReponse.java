package com.example.hvmilkteaapi.payload.response;

import com.example.hvmilkteaapi.entity.User;
import com.example.hvmilkteaapi.entity.Wishlist;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserReponse {

    private User user;
    private WishlistResponse wishlistResponse;

}
