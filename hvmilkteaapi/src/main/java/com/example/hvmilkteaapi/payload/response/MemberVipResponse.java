package com.example.hvmilkteaapi.payload.response;

import com.example.hvmilkteaapi.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MemberVipResponse {

    private User user;
    private WishlistResponse wishlistResponse;

}
