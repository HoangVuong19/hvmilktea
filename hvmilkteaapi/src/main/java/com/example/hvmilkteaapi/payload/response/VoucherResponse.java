package com.example.hvmilkteaapi.payload.response;

import com.example.hvmilkteaapi.entity.Code;
import com.example.hvmilkteaapi.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VoucherResponse {

    private List<Code> codes;
    private User user;
    private String message;
    private WishlistResponse wishlistResponse;

}
