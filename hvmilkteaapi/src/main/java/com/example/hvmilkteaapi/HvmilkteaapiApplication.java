package com.example.hvmilkteaapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HvmilkteaapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HvmilkteaapiApplication.class, args);
	}

}
