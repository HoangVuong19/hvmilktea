import { Grid, Typography } from "@material-ui/core";
import GoogleMap from "../../../common/GoogleMap";

const Footer = () => {
  return (
    <Grid
      container
      style={{
        paddingTop: 10,
        paddingBottom: 5,
        backgroundColor: "#416c48",
        color: "white",
        minHeight: "calc(100vh - 577px)",
      }}
    >
      <Grid item xs={12} md={8} style={{ paddingLeft: 200 + "px" , paddingTop: 20 + "px"}}>
        <Typography>
          <b>Trụ sở chính: </b>
          Công ty CP HVMilkTea - ĐKKD: 0316 871719 do sở KHĐT TPĐN cấp lần
          đầu ngày 20/03/2023
        </Typography>
        <Typography>
          <b>Địa chỉ: </b>
          02 Quang Trung, Thạch Thang, Hải Châu, Đà Nẵng 550000, Việt Nam
        </Typography>
        <Typography>
          <b>Điện thoại: </b>
          0334564885
        </Typography>
        <Typography>
          <b>Email: </b>
          hvmilktea@HVMilkTea.com.vn
        </Typography>
      </Grid>

      <Grid
        item
        xs={12}
        md={4}
        style={{ display: "flex", justifyContent: "center" }}
      >
        <GoogleMap />
      </Grid>
    </Grid>
  );
};

export default Footer;
