import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Hidden from '@material-ui/core/Hidden';
import content1 from './../../../assets/img/content1.jpg';
import content2 from './../../../assets/img/content2.jpg';
import content6 from './../../../assets/img/content6.png';
import content3 from './../../../assets/img/content3.png';
import { Button } from '@material-ui/core';

const useStyles = makeStyles({
    card: {
        display: 'flex',
    },
    cardDetails: {
        flex: 1,
    },
    cardMedia: {
        width: 160,
    },
    btnOrder: {
        color: "#0c713d",
        fontSize: 16,
        border: '1px solid',
        paddingRight: 10,
        paddingLeft: 10,
        '&:hover': {
            backgroundColor: '#0c713d',
            color: 'white'
        }
    }
});

const Content = () => {
    const classes = useStyles();

    return (
        <div style={{ paddingLeft: 30 + 'px', paddingRight: 30 + 'px', backgroundColor: '#ffffff' }}>

            {/* Content 1 */}
            <Grid item xs={12} md={12} style={{ paddingTop: 40 + 'px' }}>
                <Card className={classes.card} style={{ boxShadow: 'none' }}>
                    <div className={classes.cardDetails}>
                        <CardContent>
                            <Typography component="h2" variant="h5">
                                TỪ NHỮNG MẦM TRÀ, CHÚNG TÔI TẠO RA NIỀM ĐAM MÊ
                            </Typography>
                            <Typography variant="subtitle1" paragraph>
                                Trải qua hơn 50 năm chắt chiu tinh hoa từ những búp trà xanh và hạt cà phê thượng hạng cùng mong muốn mang lại cho khách hàng những trải nghiệm giá trị nhất khi thưởng thức, HVMilktea liên tục là thương hiệu tiên phong với nhiều ý tưởng sáng tạo đi đầu trong ngành trà và cà phê.
                                Chúng tôi tin rằng từng sản phẩm trà và cà phê sẽ càng thêm hảo hạng khi được tạo ra từ sự phấn đấu không ngừng cùng niềm đam mê. Và chính kết nối dựa trên niềm tin, sự trung thực và tin yêu sẽ góp phần mang đến những nét đẹp trong văn hóa thưởng trà và cà phê ngày càng bay cao, vươn xa.
                            </Typography>
                            <Button size="small" color="primary" className={classes.btnOrder}>
                                Xem thêm
                            </Button>
                        </CardContent>
                    </div>
                    <Hidden xsDown>
                        <CardMedia className={classes.cardMedia} image={content1} title="bannertrangchu" style={{ width: 500 + 'px' }} />
                    </Hidden>
                </Card>
            </Grid>

            {/* Content 2 */}
            <Grid item xs={12} md={12} style={{ paddingTop: 80 + 'px' }}>
                <Card className={classes.card} style={{ boxShadow: 'none' }}>
                    <Hidden xsDown>
                        <CardMedia className={classes.cardMedia} image={content2} title="bannertrangchu" style={{ width: 500 + 'px' }} />
                    </Hidden>
                    <div className={classes.cardDetails}>
                        <CardContent>

                            <Typography variant="subtitle1" paragraph>
                                Chúng tôi tin rằng từng sản phẩm trà và cà phê sẽ càng thêm hảo hạng khi
                                được tạo ra từ sự phấn đấu không ngừng cùng niềm đam mê. Và chính kết
                                nối dựa trên niềm tin, sự trung thực và tin yêu góp phần mang đến những
                                nét đẹp trong văn hóa thưởng trà & cà phê ngày càng bay cao và vươn xa.
                                Tiếp nối nỗ lực, từ một cửa hàng đầu tiên, đến nay, HVMilktea xây dựng
                                hơn 10 cửa hàng trên khu vực TP.HCM, Đà Nẵng, Hà Nội phục vụ những thức uống tươi
                                ngon từ trà và cà phê. Không dừng lại tại đó, chúng tôi tiếp tục định hướng phát triển
                                mở rộng hệ thống cửa hàng trải dài từ Nam ra Bắc. Tăng độ phủ của sản phẩm đến tất cả
                                các hệ thống: siêu thị, cửa hàng tiện lợi…
                            </Typography>
                            <Button size="small" color="primary" className={classes.btnOrder}>
                                Xem thêm
                            </Button>
                        </CardContent>
                    </div>
                </Card>
            </Grid>

            {/* Content 3 */}
            <Grid item xs={12} md={12} style={{ paddingTop: 80 + 'px' }}>
                <Card className={classes.card} style={{ boxShadow: 'none' }}>
                    <div className={classes.cardDetails}>
                        <CardContent>
                            <Typography variant="subtitle1" paragraph>
                                Hoạt động với phương châm “Chất lượng khởi nguồn từ đam mê”, cùng nhau,
                                chúng tôi đã và đang viết tiếp câu chuyện của một thương hiệu Việt giàu
                                truyền thống, kết nối tinh túy của nhiều thập niên kinh nghiệm và không
                                ngừng chắt lọc, làm mới mình trong bước chuyển thời gian để trở thành
                                thương hiệu luôn gắn bó với nhiều thế hệ khách hàng.
                            </Typography>
                            <Button size="small" color="primary" className={classes.btnOrder}>
                                Xem thêm
                            </Button>
                        </CardContent>
                    </div>
                    <Hidden xsDown>
                        <CardMedia className={classes.cardMedia} image={content3} title="bannertrangchu" style={{ width: 500 + 'px' }} />
                    </Hidden>
                </Card>
            </Grid>

            {/* Content 4 */}
            <Grid item xs={12} md={12} style={{ paddingTop: 80, paddingBottom: 80 }}>
                <Card className={classes.card} style={{ boxShadow: 'none' }}>
                    <Hidden xsDown>
                        <CardMedia className={classes.cardMedia} image={content6} title="bannertrangchu" style={{ width: 700 + 'px', height: 200 + 'px' }} />
                    </Hidden>
                    <div className={classes.cardDetails}>
                        <CardContent>
                            <Typography component="h2" variant="h5">
                                TẦM NHÌN
                            </Typography>
                            <Typography variant="subtitle1" paragraph>
                                Với khát vọng không ngừng mở rộng thị trường - phát triển bền
                                vững, HVMilktea phấn đấu trở thành doanh nghiệp sản xuất và xuất khẩu
                                trà, cà phê có giá trị cao hàng đầu tại Việt Nam.
                            </Typography>
                            <Button size="small" color="primary" className={classes.btnOrder}>
                                Xem thêm
                            </Button>
                        </CardContent>
                    </div>
                </Card>
            </Grid>
        </div>
    )
}

export default Content