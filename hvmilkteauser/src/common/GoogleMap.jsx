import { makeStyles } from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((theme) => ({
    map: {
        border: 0,
        width: 400,
        heigth: 200,
        [theme.breakpoints.down("md")]: {
            width: 300,
            heigth: 200,
        },
    }
}))

const GoogleMap = () => {

    const classes = useStyles();

    return (
        <div>
            <iframe title="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4429.391108356319!2d108.21935110441217!3d16.075865015628867!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314219bcc65d7ee7%3A0xa61181e4e38b5b6d!2zQ8O0bmcgdmnDqm4gcGjhuqduIG3hu4FtIDAyIFF1YW5nIFRydW5n!5e0!3m2!1svi!2s!4v1681654733199!5m2!1svi!2s" className={classes.map} loading="lazy"></iframe>
        </div>
    );
}

export default GoogleMap